class Settings():

    # Path to the FASTA file containing the sequences within which
    # CRISPR targets will need to be identified
    subject_fasta = "/home/sba/bioinf/temp/myc.fa"

    # Patterns for extraction of CRISPR targets
    # Typical CRISPR targets will have one of the following patterns:
    # (1) g(n)xngg
    # (2) ccn(n)xc
    patterns = {
        "[g-(n)x]-ngg": "(?=((g.{{{word_len}}}).gg))",
        "ccn-[(n)x-c]": "(?=(cc.(.{{{word_len}}}c)))"
    }

    # Bounds for sequence lengths
    min_word_length = 15
    max_word_length = 20

    # Specify the minimum and maximum sizes of periods used when
    # building a region to tag
    min_period = 30
    max_period = 100

    # Only consider regions that have at least min_tag_units tagging units
    min_tag_units = 21

    # Number of iterations to perform to search for best possible
    # combination of targets
    iterations = 100