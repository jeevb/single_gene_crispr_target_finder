from collections import defaultdict
from modules.ape_record import TargetRecord


class Targets():

    def __init__(self, query):
        self.query = query

        self.target_record = TargetRecord(self.query.source_seq)
        self.members = defaultdict(int)

    def add_target(self, target):
        self.members[target.sequence] += 1
        self.target_record.add_feature(target.start, target.end)

    @property
    def total(self):
        return sum(self.members.values())

    @property
    def unique(self):
        return len(self.members)

    def __str__(self):
        template = "\t{seq: <35}{count: 3}"

        return "\n".join(
            [template.format(seq=s, count=c) for s, c in self.members.items()]
        )
