from modules.targets import Targets


class ChainFinder():

    def __init__(self, min_period, max_period, min_tag_units, iterations):
        self.min_period = min_period
        self.max_period = max_period
        self.min_tag_units = min_tag_units
        self.iterations = iterations

    def _calc_node_data(self, query):
        query.reset_nodes()

        for idx, node in enumerate(query.nodes):
            for i in xrange(idx):
                curr_node = query.nodes[i]

                if (node.start-self.max_period <= curr_node.start <=
                    node.start-self.min_period):
                    node.add_input(curr_node)

            node.extend_chain()

    def _build_targets_list(self, query):
        last_node = max(query.nodes,
                        key=lambda n: (len(n.chain), n.score)).chain

        targets = Targets(query)
        for target in last_node:
            targets.add_target(target)

        return targets

    def find_targets(self, query):
        for itr in xrange(self.iterations):
            self._calc_node_data(query)
            itr_result = self._build_targets_list(query)

            if (not query.targets or
                            0 < itr_result.unique < query.targets.unique):
                query.targets = itr_result