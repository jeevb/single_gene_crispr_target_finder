import random

from collections import Counter


class Node():

    def __init__(self, data):
        self.start, self.end, self.sequence = data

        self.inputs = []
        self.chain = []
        self.score = 0

    @property
    def _input(self):
        max_input_score = max(i.score for i in self.inputs)
        return random.choice([i for i in self.inputs
                              if i.score == max_input_score])

    def _update_score(self):
        chain_seqs = [i.sequence for i in self.chain]

        for _, count in Counter(chain_seqs).items():
            self.score += count**2

    def reset(self):
        self.inputs[:] = []
        self.chain[:] = []
        self.score = 0

    def add_input(self, node):
        self.inputs.append(node)

    def extend_chain(self):
        if self.inputs:
            self.chain = list(self._input.chain)

        self.chain.append(self)
        self._update_score()