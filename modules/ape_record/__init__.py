from Bio import SeqFeature
from Bio import SeqIO
from Bio.Alphabet import generic_dna
from Bio.Seq import Seq
from Bio.SeqRecord import SeqRecord


class ApeRecord(object):

    identifier = 0

    @classmethod
    def _new_id(cls):
        cls.identifier += 1
        return cls.identifier

    def __init__(self, sequence):
        self.record = SeqRecord(Seq(sequence, generic_dna))

    @staticmethod
    def _label_feature(feature):
        pass

    def add_feature(self, start, end):
        feature = SeqFeature.SeqFeature(
            SeqFeature.FeatureLocation(
                SeqFeature.ExactPosition(start),
                SeqFeature.ExactPosition(end)
            ),
            type="misc_feature"
        )

        self._label_feature(feature)
        self.record.features.append(feature)

    def write(self, filename):
        with open(filename, "w") as ohandle:
            SeqIO.write(self.record, ohandle, "gb")


class NodeRecord(ApeRecord):

    @staticmethod
    def _label_feature(feature):
        new_id = NodeRecord._new_id()
        feature.qualifiers["label"] = "Node %i" % new_id
        feature.qualifiers["ApEinfo_fwdcolor"] = \
            "#ffa500" if new_id % 2 else "#808080"


class TargetRecord(ApeRecord):

    @staticmethod
    def _label_feature(feature):
        feature.qualifiers["label"] = "Target %i" % TargetRecord._new_id()
        feature.qualifiers["ApEinfo_fwdcolor"] = "#ff6666"
