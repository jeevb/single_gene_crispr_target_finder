import re


class WordMiner():

    def __init__(self, pattern, min_word_length, max_word_length):
        self.pattern = pattern

        self.min_word_length = min_word_length
        self.max_word_length = max_word_length

    def find_words(self, sequence):
        for i in xrange(self.min_word_length, self.max_word_length+1):
            for match in re.compile(
                    self.pattern.format(word_len=i-1)
            ).finditer(sequence.lower()):
                yield (
                    match.start(1),
                    match.end(1),
                    match.group(2)
                )