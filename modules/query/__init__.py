from modules.ape_record import NodeRecord


class Query():

    def __init__(self, seq_record, pattern):

        self.source = seq_record.name
        self.source_seq = str(seq_record.seq).lower()
        self.source_len = len(self.source_seq)

        self.pattern = pattern

        self.node_record = NodeRecord(self.source_seq)
        self.nodes = []
        self.targets = None

    def add_node(self, node):
        self.nodes.append(node)
        self.node_record.add_feature(node.start, node.end)

    def sort_nodes(self):
        self.nodes.sort(key=lambda n: n.start)

    def reset_nodes(self):
        for node in self.nodes:
            node.reset()

    @property
    def node_count(self):
        return len(self.nodes)
