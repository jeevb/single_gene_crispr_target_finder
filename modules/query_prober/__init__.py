from Bio import SeqIO
from modules.query import Query
from modules.chain_finder import ChainFinder
from modules.node import Node
from modules.word_miner import WordMiner


class QueryProber():

    def __init__(self, config):
        self.config = config

        self.cf = ChainFinder(
                self.config.min_period,
                self.config.max_period,
                self.config.min_tag_units,
                self.config.iterations
            )

    def _probe_query(self, pattern):
        pattern_desc, pattern_format = pattern

        wm = WordMiner(
            pattern_format,
            self.config.min_word_length,
            self.config.max_word_length
        )

        for record in SeqIO.parse(open(self.config.subject_fasta, "rU"),
                                  "fasta"):
            query = Query(record, pattern_desc)

            for word in wm.find_words(str(record.seq)):
                query.add_node(Node(word))

            query.sort_nodes()

            if query.node_count < self.config.min_tag_units:
                continue

            self.cf.find_targets(query)

            if query.targets:
                yield query

    def probed_queries(self):
        for pattern in self.config.patterns.items():
            for query in self._probe_query(pattern):
                yield query