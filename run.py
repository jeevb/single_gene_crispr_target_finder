from modules.query_prober import QueryProber
from settings import Settings

def main():
    config = Settings()
    qp = QueryProber(config)

    for probed_query in qp.probed_queries():
        prefix = "%s_%s" % (probed_query.source, probed_query.pattern)
        probed_query.node_record.write(prefix + "_all_nodes.ape")
        probed_query.targets.target_record.write(prefix + ".ape")

        print "Query Name:", probed_query.source
        print "Query Length:", probed_query.source_len
        print "Pattern Format:", probed_query.pattern
        print "Total Targets:", probed_query.targets.total
        print "Unique Sequence Count:", probed_query.targets.unique
        print "Unique Sequences:\n", str(probed_query.targets)
        print "\n"


if __name__ == "__main__":
    main()
